﻿Imports MySql.Data.MySqlClient
Imports System
Imports System.IO

Public Class Utilisateurs
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim queryString As String = "SELECT  `IdUser`,  `Nom`,  `Prenom`,  `Remarque`,  `Company` FROM `factboumediene`.`utilisateur` LIMIT 1000;"
            Using con
                con.ConnectionString = DaoService.ConnectionString
                Dim command As New MySqlCommand(queryString, con)
                con.Open()
                DaoService.reader = command.ExecuteReader()
                Dim dt As New DataTable
                dt.Load(DaoService.reader)
                GridView1.DataSource = dt
                GridView1.DataBind()
                DaoService.reader.Close()
            End Using


        Catch ex As Exception
            MsgBox("Erreur de chargement de la liste des Utilisateurs")
        Finally

            con.Close()
        End Try
    End Sub

   
    Protected Sub LoadUsers_Click(ByVal sender As Object, ByVal e As EventArgs) Handles LoadUsers.Click
        Dim Response As System.Web.HttpResponse = System.Web.HttpContext.Current.Response
        Try
            ' create an API client instance
            Dim client As New pdfcrowd.Client("mvcpatern", "ae57fd6af6f93897ebc69f6ecb86f5b3")

            ' convert a web page and write the generated PDF to a memory stream
            Dim Stream As New MemoryStream
            client.convertURI("http://mbworks.jitabs.com/", Stream)

            ' set HTTP response headers
            Response.Clear()
            Response.AddHeader("Content-Type", "application/pdf")
            Response.AddHeader("Cache-Control", "max-age=0")
            Response.AddHeader("Accept-Ranges", "none")
            Response.AddHeader("Content-Disposition", "attachment; filename=google_com.pdf")

            ' send the generated PDF
            Stream.WriteTo(Response.OutputStream)
            Stream.Close()
            Response.Flush()
            Response.End()
        Catch why As pdfcrowd.Error
            Response.Write(why.ToString())
        End Try

    End Sub
End Class