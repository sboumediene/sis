﻿Public Class Maitre
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim comany As String = DaoService.getCompany()
        lblUser.Text = DaoService.id & "-" & comany

    End Sub

    Protected Sub Deconnect_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Deconnect.Click
        DaoService.id = ""
        Response.Redirect("Login.Aspx")
    End Sub
End Class