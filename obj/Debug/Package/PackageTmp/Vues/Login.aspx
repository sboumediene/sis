﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Login.aspx.vb" Inherits="MBWORKS.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 164px;
        }
        .style2
        {
            width: 316px;
        }
        #Text1
        {
            width: 313px;
        }
        #txtId
        {
            width: 321px;
        }
        .style3
        {
            width: 164px;
            height: 25px;
        }
        .style4
        {
            width: 316px;
            height: 25px;
        }
        .style5
        {
            height: 25px;
        }
        #txtId0
        {
            width: 314px;
        }
        #txtpass
        {
            width: 319px;
        }
        #txtid0
        {
            width: 321px;
        }
        .style6
        {
            width: 917px;
        }
        .style7
        {
            width: 279px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <br />
        <br />
        <table style="width:100%;">
            <tr>
                <td class="style6">
                    &nbsp;</td>
                <td class="style7">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style6">
                    <img alt="" src="../Images/jitabs.png" 
                        style="height: 135px; width: 270px" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <img alt="" src="../Images/mbworks.jpg" 
                        style="height: 135px; width: 270px" /></td>
                <td class="style7">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style6" 
                    style="text-decoration: underline; font-size: xx-large; font-family: 'Courier New', Courier, monospace; font-style: italic; font-weight: bold">
                    Ouvrez une session pour accéder à votre espace
                </td>
                <td class="style7">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
        <br />
        <br />
        <br />
        <br />
        <asp:Label ID="lblMsg" runat="server" ForeColor="#FF3300"></asp:Label>
        <br />
        <table style="width:100%;">
            <tr>
                <td class="style1" 
                    style="background-color: #C0C0C0; font-family: 'Arial Black'; font-size: large; font-style: normal; text-decoration: underline; font-weight: bold; color: #000000">
                    <asp:Label ID="Label1" runat="server" Text="Identifiant"></asp:Label>
                </td>
                <td class="style1" 
                    style="background-color: #C0C0C0; font-family: 'Arial Black'; font-size: large; font-style: normal; text-decoration: underline; font-weight: bold; color: #000000">
                    <asp:TextBox ID="txtid" runat="server" Width="315px"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style1" 
                    style="background-color: #C0C0C0; font-family: 'Arial Black'; font-size: large; font-style: normal; text-decoration: underline; font-weight: bold; color: #000000">
                    <asp:Label ID="Label2" runat="server" Text="Mot de passe"></asp:Label>
                </td>
                <td class="style1" 
                    style="background-color: #C0C0C0; font-family: 'Arial Black'; font-size: large; font-style: normal; text-decoration: underline; font-weight: bold; color: #000000">
                    <asp:TextBox ID="txtpass" runat="server" Width="315px" TextMode="Password"></asp:TextBox>
                </td>
                <td class="style5">
                </td>
            </tr>
            <tr>
                <td class="style1">
                    &nbsp;</td>
                <td class="style2">
                    <asp:LinkButton ID="btConnect" runat="server">Ouvrir session</asp:LinkButton>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
